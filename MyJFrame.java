package programming2_assignment6lavigne;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Wyatt
 */
public class MyJFrame extends JFrame {

    JMenuBar menubar;
    JMenu mc1, mc2, mc3, emc;
    JMenuItem mi;
    static JButton drawing = new JButton("Drawing");
    static JButton image = new JButton("Image");
    static JButton close = new JButton("Close");
    static JButton browser = new JButton("Browser");

    public MyJFrame(String title) {
        super(title);
        menubar = new JMenuBar();
        setJMenuBar(menubar);
        buildMenu();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                JOptionPane.showMessageDialog(null, "The window is closing.");
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        MyJFrame frame = new MyJFrame("My First GUI 2015");
        frame.setSize(600, 600);
        frame.setVisible(true);
        frame.setLayout(new GridLayout(3, 3, 50, 50));
        frame.add(drawing);
        frame.add(close);
        frame.add(browser);
        frame.add(image);
        drawing.setVisible(true);
        close.setVisible(true);
        browser.setVisible(true);
        image.setVisible(true);        
        drawing.addActionListener(new Actions());
        image.addActionListener(new Actions());
        close.addActionListener(new Actions());
        browser.addActionListener(new Actions());
    }

    public void buildMenu() {
        mc1 = new JMenu(Constants.MENU[0]);
        mc2 = new JMenu(Constants.MENU[1]);
        menubar.add(mc1);
        for (int j = 0; j < Constants.FILEMENU.length; j++) {
            mi = new JMenuItem(Constants.FILEMENU[j]);
            mi.addActionListener(new Actions());
            mc1.add(mi);
            menubar.add(mc1);
            switch (j) {
                case 1:
                    mc1.addSeparator();
                case 2:
                    mc1.addSeparator();
                default:
                    break;
            }
        }

        for (int j = 0; j < Constants.TOOLMENU.length; j++) {
            mi = new JMenuItem(Constants.TOOLMENU[j]);
            mi.addActionListener(new Actions());
            mc2.add(mi);
            emc = new JMenu("Edit");
            switch (j) {
                case 1:
                    for (String EDITMENU : Constants.EDITMENU) {
                        mi = new JMenuItem(EDITMENU);
                        mi.addActionListener(new Actions());
                        emc.add(mi);
                        mc2.add(emc);
                    }
                    break;
            }
        }
        menubar.add(mc2);
        menubar.add(new JMenu("Help"));
    }
}

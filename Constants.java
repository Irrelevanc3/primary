/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programming2_assignment6lavigne;

/**
 *
 * @author Wyatt
 */
public interface Constants {
    String[] MENU = {"File", "Tool", "Help"};
    String[] FILEMENU = {"New", "List Files", "Save As", "Close"};
    String[] TOOLMENU = {"Sort", "Search"};
    String[] EDITMENU = {"Copy", "Paste"};
}

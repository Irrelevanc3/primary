
package programming2_assignment6lavigne;
import javax.swing.JFrame;
import java.awt.Polygon;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;
import javax.swing.JComponent;
/**
 *
 * @author Wyatt
 */
public class MyGraphicsComponent extends JComponent {
    JFrame graphic = new JFrame();
    Graphics g = graphic.getGraphics();
    Graphics2D g2 = (Graphics2D) graphic.getGraphics();
    int[] xStar = {0,25,50,75,100};
    int[] yStar = {100,150,50,150,100};
    Polygon star = new Polygon(xStar, yStar, 5);
    int[] xHouse = {25, 25, 100, 100, 150, 200, 200, 275, 275};
    int[] yHouse = {300, 225, 300, 225, 150, 225, 300, 225, 300};
    Polygon homeFront = new Polygon(xHouse, yHouse, 9);
    Font fontChoice = new Font("Comic Sans", Font.PLAIN, 12);    
    @Override
    public void paintComponent(Graphics g){
     g2.draw(star);
     g2.draw(homeFront);
     g2.setFont(fontChoice);
     g2.fillRect(100, 225, 100, 75);
     g2.drawString("My House", 150, 330);
     graphic.setVisible(true);
    }
}

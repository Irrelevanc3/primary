package programming2_assignment6lavigne;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
//With thanks to Professor Joslyn Smith, who provided this code in its original form.
class Actions implements ActionListener
{
	DisplayText dt;
        String copied = "";
        @Override
	public void actionPerformed(ActionEvent e){
		if (e.getActionCommand().equalsIgnoreCase("Browser"))
		{
			Browser brow = new Browser();
                        brow.browse();
		}
		else if(e.getActionCommand().equalsIgnoreCase("Open"))
		{
			BasicFile f = new BasicFile();
                        try{
			dt = new DisplayText( f.getName(), f.getContents());
                        }
                        catch(IOException ex){
                        }
			System.out.println(f.getName() ); 
                }
                        else if(e.getActionCommand().equalsIgnoreCase("Copy")){
                            copied = dt.selectText();
                                } //Implement the Copy – Paste option, that it can copy from one window and paste to another window.
                        else if(e.getActionCommand().equalsIgnoreCase("Paste")){
                            DisplayText.text.insert(copied, DisplayText.text.getCaretPosition());
                        }
                        else if(e.getActionCommand().equalsIgnoreCase("New")){
                            File newFile = new File("Untitled");
                            dt = new DisplayText(newFile.getName(), "");
                        }
                        else if(e.getActionCommand().equalsIgnoreCase("List Files")){
                            BasicFile f = new BasicFile();
                        }
                        else if(e.getActionCommand().equalsIgnoreCase("Close")){
                            JOptionPane.showMessageDialog(null, "The window is closing.");
                            System.exit(0);
                        }
                        else if(e.getActionCommand().equalsIgnoreCase("Drawing")){
                            MyGraphicsComponent gc = new MyGraphicsComponent();
                            gc.paintComponent(gc.getGraphics());
                        }
                    }
}
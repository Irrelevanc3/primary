package programming2_assignment6lavigne;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import javax.swing.event.HyperlinkEvent;
import java.awt.event.ActionEvent;
import java.net.URL;
/**
 *
 * @author Wyatt, credit to Professor Joslyn Smith for providing much of the instructions.
 */
public class Browser {
    JTextField enter = new JTextField("http://");
     MyJFrame frame = new MyJFrame("Browser");
     JEditorPane contents = new JEditorPane();
     Container c = frame.getContentPane();
    public void browse(){
    contents.setEditable(false);
    enter.addActionListener((ActionEvent e) -> {
        getThePage(e.getActionCommand());
    });
contents.addHyperlinkListener((HyperlinkEvent e) -> {
    if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
        getThePage(e.getURL().toString());
    });
JScrollPane scroll = new JScrollPane();
scroll.add(contents);
c.add(enter, BorderLayout.NORTH);
c.add(scroll, BorderLayout.CENTER);
frame.setBounds(0, 0, 500, 500);
frame.setVisible(true);
    }
    public void getThePage(String location){
        
            try{
                URL url = new URL(location); //Maybe working.
                Cursor cur = new Cursor(java.awt.Cursor.WAIT_CURSOR); //Working.
                frame.setCursor(cur); //Working.
                contents.setPage(url); //Doesn't appear to be working.
                enter.setText(location); //Working?
                frame.setCursor(null); //Working.
            }
            catch (IOException io){
                JOptionPane.showMessageDialog(c, "Error: cannot access specified URL", "Bad URL", JOptionPane.ERROR_MESSAGE);
                browse();
            }
            }
        }
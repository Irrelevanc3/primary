package programming2_assignment6lavigne;
import javax.swing.JFileChooser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.*;
import javax.swing.JOptionPane;
import java.io.FileReader;

public class BasicFile {

    File f;
    File h = new File("Output.txt");
    boolean printFlag = true;

   public BasicFile()
		{
				JFileChooser choose = new JFileChooser(".");
				int status = choose.showOpenDialog(null);
				
				try
				{
						if (status != JFileChooser.APPROVE_OPTION) 
							throw new IOException();
						f = choose.getSelectedFile();
						if (!f.exists()) 
							throw new FileNotFoundException();
				}
				catch(FileNotFoundException e)
				{
						display(e.toString(), "File not found ....");
				}
				catch(IOException e)
				{
						display(e.toString(),  "Approve option was not selected");
				}
		}

    void display(String msg, String s) {
        JOptionPane.showMessageDialog(null, msg, s, JOptionPane.ERROR_MESSAGE);
    }

    public File copyFile() throws FileNotFoundException, IOException {
        FileInputStream fin = new FileInputStream(f);
        DataInputStream din = new DataInputStream(fin);
        File g = new File("Backup.txt");
        FileOutputStream fout = new FileOutputStream(g);
        DataOutputStream dout = new DataOutputStream(fout);
            for (long i = 0; i < f.length(); i++) {
                dout.writeByte(din.readByte());
            }
        
        return g;
    }

    public String getAbsolutePath() {
        return f.getAbsolutePath();
    }

    public String getContents() throws FileNotFoundException, IOException {
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        StringBuffer buf = new StringBuffer("");
        while (lnr.readLine() != null) {
            buf.append(lnr.readLine()).append("\n");
        }
        return buf.toString();
    }

    public String retrievePathAndFiles() {
        StringBuffer path = new StringBuffer("");
        File temp;
        temp = new File(f.getParent());
        String[] tempHolder;
        if (f.getParent() == null) {
            path.append(f.list());
        } else {
            while (temp.getParent() != null) {
                temp = new File(temp.getParent());
                if (temp.isDirectory()) {
                    tempHolder = temp.list();
                    for (String holder : tempHolder) {
                        path.append(holder).append("\n");
                    }
                } else {
                    path.append(temp.getAbsolutePath()).append("\n");
                }
            }
        }
        return path.toString();
    }

    public long getFileSize() {
        return f.length();
    }

    public int getLineCount() throws FileNotFoundException, IOException { //With thanks to Professor Joslyn Smith for TokeniZer.java, from which this method was derived.
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        StreamTokenizer token = new StreamTokenizer(lnr);
        String[] word = new String[32];
        int number, lineCount;
        token.eolIsSignificant(true);
        while (token.ttype != StreamTokenizer.TT_EOF) {
            token.nextToken();
        }
        lineCount = token.lineno();
        return lineCount;
    }

    public int getWordCount() throws FileNotFoundException, IOException { //With thanks to Professor Joslyn Smith for TokeniZer.java, from which this method was derived.
        int wordCount = 0;
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        StreamTokenizer token = new StreamTokenizer(lnr);
        String[] word = new String[32];
        int number;
        token.eolIsSignificant(true);

        token.nextToken();

        while (token.ttype != StreamTokenizer.TT_EOF) {
            switch (token.ttype) {
                case StreamTokenizer.TT_WORD: {
                    wordCount++;
                    break;
                }
                default:
                    break;
            }
            token.nextToken();
        }
        return wordCount;
    }

    public int getCharCount() throws FileNotFoundException, IOException { //With thanks to Professor Joslyn Smith for TokeniZer.java, from which this method was derived.
        int charCount = 0;
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        StreamTokenizer token = new StreamTokenizer(lnr);
        String[] word = new String[1];
        token.eolIsSignificant(false);

        token.nextToken();
        while (token.ttype != StreamTokenizer.TT_EOF) {
            switch (token.ttype) {
                case StreamTokenizer.TT_WORD: {
                    charCount += token.toString().length();
                    break;
                }
                case StreamTokenizer.TT_NUMBER: {
                    charCount += token.toString().length();
                    break;
                }
                default:
                    break;
            }
            token.nextToken();
        }
        return charCount;
    }

    public int getNumCount() throws FileNotFoundException, IOException { //With thanks to Professor Joslyn Smith for TokeniZer.java, from which this method was derived.
        int numCount = 0;
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        StreamTokenizer token = new StreamTokenizer(lnr);
        String[] word = new String[32];
        int number;
        token.eolIsSignificant(true);

        token.nextToken();

        while (token.ttype != StreamTokenizer.TT_EOF) {
            switch (token.ttype) {
                case StreamTokenizer.TT_NUMBER: {
                    numCount++;
                    break;
                }
                default:
                    break;
            }
            token.nextToken();
        }
        return numCount;
    }

    public String getName() {
        return f.getName();
    }

    public String searchFile(String query) throws FileNotFoundException, IOException {
        FileReader fred = new FileReader(f);
        LineNumberReader lnr = new LineNumberReader(fred);
        String s;
        StringBuffer found = new StringBuffer();
        if (!query.equals("")) {
            while ((s = lnr.readLine()) != null) {
                int lineNum = lnr.getLineNumber();
                lnr.readLine();
                if (s.contains(query)) {
                    found.append(lineNum).append(": ").append(s).append("\n");
                }
            }
            if (!found.toString().equals("") && printFlag) {
                JOptionPane.showMessageDialog(null, "Your search for " + query + " found: " + found);
                printFlag = false;
            }
            return "Your search for " + query + " found: " + found.toString();
        } else {
            if (printFlag == true && found.toString().equals("") && !query.equals("")) {
                JOptionPane.showMessageDialog(null, "The search found: nothing.");
                printFlag = false;
            }
            return "Your search for " + query + " found: nothing.";
        }
    }

    public File outputWriter(File h, String query) throws FileNotFoundException, IOException {
        String filepath = getAbsolutePath();
        String directoryList = retrievePathAndFiles();
        String s = f.length() + " bytes" + "\n" + f.getAbsolutePath();
        String fn = f.getName();
        long size = getFileSize();
        int words = getWordCount();
        int chars = getCharCount();
        int lines = getLineCount();
        int numbers = getNumCount();
        FileWriter writer = new FileWriter(h);
        writer.write(filepath + "\n");
        writer.write(directoryList + "\n");
        writer.write((int) size + "\n");
        writer.write(words + "\n");
        writer.write(chars + "\n");
        writer.write(lines + "\n");
        writer.write(numbers + "\n");
        writer.write(s + "\nFilename: " + fn + "\nDirectory: " + retrievePathAndFiles() + "\nLine Count: " + getLineCount() + "\n Word Count: " + getWordCount() + "\n Character Count: " + getCharCount() + "\n Number Count: " + getNumCount() + "\n" + searchFile(query));
        writer.close();
        return h;
    }
}

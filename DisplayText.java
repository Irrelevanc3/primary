package programming2_assignment6lavigne;
import javax.swing.*;
import java.awt.*;
import javax.swing.event.CaretEvent;
/**
 *
 * @author Wyatt
 * //With thanks to Professor Joslyn Smith, who provided this code in its original form.
 */
public class DisplayText{
    static JTextArea text;
    static String selectedText;
public DisplayText(String title, String info)
   {
		MyJFrame f = new MyJFrame(title);
		Container c = f.getContentPane();

		text = new JTextArea(info);
                text.setEditable(true);

		JScrollPane sp = new JScrollPane(text);
		c.add( sp );

      	f.setBounds(100,200, 500, 400 );
	   	f.setVisible(true);
                
                
   }
public String selectText(){ //captures any text that is selected by the mouse.
    text.addCaretListener((CaretEvent ce) -> {
        int dot=ce.getDot();
        int mark=ce.getMark();
        
        if(dot!=mark)
            selectedText = text.getSelectedText();
        else selectedText = null;
    } //This anonymous class derives its code from an example provided by one "JavaTechnical" of StackOverflow.
    );
    return text.getSelectedText();
}
public void insertText(int pos, String insertedText){ //inserts the text that was capture by selectText() and places the text wherever in the text you want it to be placed.
    text.insert(insertedText, pos);
}
}

